// First we import the express module and then create an instance of express
const express = require('express')
const app = express()
const testRoute = require('./routes/testRoute')

app.use(express.json())

app.use('/db', testRoute)
// We then create a route for the root of our application
app.get('/', (req, res) => {
  res.send('Hello World')
})



//We then listen on port 3000
app.listen(3000, () => {
  console.log('Server running on port 3000...')
})
